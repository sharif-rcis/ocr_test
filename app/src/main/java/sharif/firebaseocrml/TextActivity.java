package sharif.firebaseocrml;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;

import sharif.firebaseocrml.helpers.MyHelper;
import sharif.firebaseocrml.helpers.VisionImage;


public class TextActivity extends BaseActivity implements View.OnClickListener {
	private Bitmap mBitmap;
	private ImageView mImageView;
	private TextView mTextView;
	VisionImage visionImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_text);

		mTextView = findViewById(R.id.text_view);
		mImageView = findViewById(R.id.image_view);
		findViewById(R.id.btn_device).setOnClickListener(this);
		findViewById(R.id.btn_cloud).setOnClickListener(this);
        visionImage = new VisionImage();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_device:
				if (mBitmap != null) {
					runTextRecognition();
				}
				break;
			case R.id.btn_cloud:
				if (mBitmap != null) {
					runCloudTextRecognition();
				}
				break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			switch (requestCode) {
				case RC_STORAGE_PERMS1:
				case RC_STORAGE_PERMS2:
					checkStoragePermission(requestCode);
					break;
				case RC_SELECT_PICTURE:
					Uri dataUri = data.getData();
					String path = MyHelper.getPath(this, dataUri);
					if (path == null) {
						mBitmap = MyHelper.resizeImage(imageFile, this, dataUri, mImageView);
					} else {
						mBitmap = MyHelper.resizeImage(imageFile, path, mImageView);
					}
					if (mBitmap != null) {
						mTextView.setText(null);
						mImageView.setImageBitmap(mBitmap);
					}
					break;
				case RC_TAKE_PICTURE:
					/*mBitmap = MyHelper.resizeImage(imageFile, imageFile.getPath(), mImageView);
					mBitmap = (Bitmap) data.getExtras().get("data");
                    Bitmap photo = (Bitmap) data.getExtras().get("data");*/

                    String imagePath = CameraActivity.parsePath(data);
					mBitmap = getResizedBitmap(getBitmap(imagePath),480,360);
					mImageView.setImageBitmap(mBitmap);

					break;
			}
		}
	}

    private void runTextRecognition() {

        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(mBitmap);

		FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance().getOnDeviceTextRecognizer();
		detector.processImage(image).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
			@Override
			public void onSuccess(FirebaseVisionText texts) {
				processTextRecognitionResult(texts);
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				e.printStackTrace();
			}
		});
	}

	private void processTextRecognitionResult(FirebaseVisionText firebaseVisionText) {
		mTextView.setText(null);
		if (firebaseVisionText.getTextBlocks().size() == 0) {
			mTextView.setText(R.string.error_not_found);
			return;
		}
		for (FirebaseVisionText.TextBlock block : firebaseVisionText.getTextBlocks()) {
			mTextView.append(block.getText());
			 //* In case you want to extract each line
			/*for (FirebaseVisionText.Line line: block.getLines()) {
				for (FirebaseVisionText.Element element: line.getElements()) {
					mTextView.append(element.getText() + " ");
				}
			}*/
		}
	}

	private void runCloudTextRecognition() {
		MyHelper.showDialog(this);

        FirebaseVisionCloudTextRecognizerOptions options = new FirebaseVisionCloudTextRecognizerOptions.Builder()
                .setLanguageHints(Arrays.asList("en", "hi","bn"))
                .build();
		FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(mBitmap);

        FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance().getCloudTextRecognizer(options);
		detector.processImage(image).addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
			@Override
			public void onSuccess(FirebaseVisionText texts) {
				MyHelper.dismissDialog();
				processCloudTextRecognitionResult(texts);
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				MyHelper.dismissDialog();
				e.printStackTrace();
			}
		});
	}

	private void processCloudTextRecognitionResult(FirebaseVisionText firebaseVisionCloudText) {
		mTextView.setText(null);
		if (firebaseVisionCloudText == null) {
			mTextView.setText(R.string.error_not_found);
			return;
		}

		mTextView.setText(firebaseVisionCloudText.getText());
		/*
		 * In case you want to get Paragraph, Word, Symbol
		StringBuilder sentenceStr = new StringBuilder();
		List<FirebaseVisionCloudText.Page> pages = firebaseVisionCloudText.getPages();
		for (int i = 0; i < pages.size(); i++) {
			FirebaseVisionCloudText.Page page = pages.get(i);
			List<FirebaseVisionCloudText.Block> blocks = page.getBlocks();
			for (int j = 0; j < blocks.size(); j++) {
				List<FirebaseVisionCloudText.Paragraph> paragraphs = blocks.get(j).getParagraphs();
				for (int k = 0; k < paragraphs.size(); k++) {
					FirebaseVisionCloudText.Paragraph paragraph = paragraphs.get(k);
					List<FirebaseVisionCloudText.Word> words = paragraph.getWords();
					for (int l = 0; l < words.size(); l++) {
						List<FirebaseVisionCloudText.Symbol> symbols = words.get(l).getSymbols();

						StringBuilder wordStr = new StringBuilder();
						for (int m = 0; m < symbols.size(); m++) {
							wordStr.append(symbols.get(m).getText());
						}
						//mTextView.append(wordStr);
						//mTextView.append(": " + words.get(l).getConfidence());
						//mTextView.append("\n");

						sentenceStr.append(wordStr).append(" ");
					}
				}
			}
		}
		mTextView.append("\n" + sentenceStr);
		*/
	}

	public Bitmap getBitmap(String path) {
		try {
			Bitmap bitmap=null;
			File f= new File(path);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;

			bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
			//mImageView.setImageBitmap(bitmap);

			return bitmap;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		// CREATE A MATRIX FOR THE MANIPULATION
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scaleWidth, scaleHeight);

		// "RECREATE" THE NEW BITMAP
		return Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
	}

}
